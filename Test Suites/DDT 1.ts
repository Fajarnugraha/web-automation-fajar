<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DDT 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>432e822d-a493-4f1b-899d-ff43f980cb7d</testSuiteGuid>
   <testCaseLink>
      <guid>6af29a84-d344-474a-ae6c-37ed977def16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DATA DRIVEN TESTING/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>51379881-e185-4e7f-93c9-20c99b0bc1c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data DDT</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>51379881-e185-4e7f-93c9-20c99b0bc1c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>f014555f-2a93-42ad-8480-e770d7cca10f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>51379881-e185-4e7f-93c9-20c99b0bc1c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>72e524c5-ff55-4b48-a472-44508f83a6d7</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
