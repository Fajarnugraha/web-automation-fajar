import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar?')

WebUI.setText(findTestObject('01 Register WEB/input_Nama_name'), 'fajar')

WebUI.setText(findTestObject('01 Register WEB/input_Tanggal lahir_birth_date'), '17-Oct-2000')

WebUI.sendKeys(findTestObject('01 Register WEB/input_Tanggal lahir_birth_date'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('01 Register WEB/input_E-Mail_email'), 'test@test.com')

WebUI.setText(findTestObject('01 Register WEB/input_Whatsapp_whatsapp'), '312313123')

WebUI.setEncryptedText(findTestObject('01 Register WEB/input_Kata Sandi_password'), 'zOlxKv8i+CFt3w6K2cfcsQ==')

WebUI.setEncryptedText(findTestObject('01 Register WEB/input_Konfirmasi kata sandi_password_confirmation'), '')

WebUI.click(findTestObject('01 Register WEB/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('01 Register WEB/button_Daftar'))

WebUI.verifyElementText(findTestObject('01 Register WEB/span_Buat Akun Baru'), 'Buat Akun Baru')

WebUI.takeScreenshot()

WebUI.closeBrowser()

