<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify 2</name>
   <tag></tag>
   <elementGuidId>c15b8e4b-a7de-42eb-9582-172712e3e2fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/div/div[2]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.buttonBanner.daftarhoverbootcamp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c78e1138-35d5-41f9-b6b0-915f5aba70d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttonBanner daftarhoverbootcamp</value>
      <webElementGuid>c0b813bd-8be1-4afb-a38b-9921eb5a8608</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>fbe98ee6-ef00-438b-b3f2-0d42f63552cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>$store.dataForm.changeClassShow(2);$store.dataForm.classId = 4;$store.dataForm.changeClass()</value>
      <webElementGuid>72915273-a735-4c02-b170-5ba8bb7c6749</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Daftar Sekarang
                            </value>
      <webElementGuid>9b9f24c7-6e54-4d89-ac9e-4c56b1a51dde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/div[@class=&quot;containerRow&quot;]/div[@class=&quot;hoveredContainer&quot;]/button[@class=&quot;buttonBanner daftarhoverbootcamp&quot;]</value>
      <webElementGuid>75e56b15-be05-406d-ae62-6adb3d4b8ad7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/div/div[2]/button</value>
      <webElementGuid>b3183e07-8ec2-4a9b-be47-58d9fe30fd8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/following::button[1]</value>
      <webElementGuid>75bf226a-cb09-4dfc-8706-1fd52bf562b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/following::button[1]</value>
      <webElementGuid>367684b6-12e0-456f-8bbf-109852aa87c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[2]/preceding::button[1]</value>
      <webElementGuid>094d7a90-9ca0-443f-ad6a-3185313acc07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::button[1]</value>
      <webElementGuid>d4ddec09-e94c-40d7-964d-5330fe20c94d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Sekarang']/parent::*</value>
      <webElementGuid>0b8048bc-aa49-422e-bbe8-15ce689a5d8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/button</value>
      <webElementGuid>b4afcb4b-729d-4877-947f-1082125e7bf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                Daftar Sekarang
                            ' or . = '
                                Daftar Sekarang
                            ')]</value>
      <webElementGuid>9e9fcbad-e709-4b32-8aef-516e5a7b2d40</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
