<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_password_confirmation</name>
   <tag></tag>
   <elementGuidId>f2b74b99-907f-462e-a93d-b25498e02985</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#password-confirm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password-confirm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>47e4c46b-9568-4657-bb6c-23a09d2a375c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password-confirm</value>
      <webElementGuid>3b0ec98a-5e70-432a-9f8d-3cd4fc095cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>2dfa0d2b-a131-41f5-a4aa-4d2593171894</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Ulangi kata sandi anda</value>
      <webElementGuid>9a07944c-a85d-4709-a943-e6d8fbfaead5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>abce3da5-948c-4c54-9c83-d1ead263d015</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>a8364f57-1c74-40c5-bf79-d9821ed70252</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>new-password</value>
      <webElementGuid>fae7beaa-b5f4-49ca-9f51-b11593613bcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password-confirm&quot;)</value>
      <webElementGuid>b3a66e2e-3375-40f1-8bcd-f7459ebfc0e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password-confirm']</value>
      <webElementGuid>493850d4-088c-4790-9d86-58e661f5b245</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/input</value>
      <webElementGuid>b49ce424-b568-4dd8-bf61-28b62604abaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password-confirm' and @type = 'password' and @placeholder = 'Ulangi kata sandi anda' and @name = 'password_confirmation']</value>
      <webElementGuid>58b0fc79-a285-4318-8938-0b02d9bd3f2e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
