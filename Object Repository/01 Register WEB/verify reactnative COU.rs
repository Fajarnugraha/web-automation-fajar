<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify reactnative COU</name>
   <tag></tag>
   <elementGuidId>178c7e80-f139-410b-9249-af781c9f6aea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id='modalEventTitle']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modalEventTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>2ac2cab0-4315-4df2-b6c1-1c16411285db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>modalEventTitle</value>
      <webElementGuid>f4069d87-ed20-4ab4-a089-6d2578555dd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Mobile Engineer with React Native</value>
      <webElementGuid>20d661ad-5c7b-4ddb-ab88-412ac686f4b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalEventTitle&quot;)</value>
      <webElementGuid>c77c7e17-6216-40b0-a2f5-be6b1eff17b0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='modalEventTitle']</value>
      <webElementGuid>01e18051-cffb-46c6-9c18-760661547bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id=' Modal_Success']/div/div[2]/h2</value>
      <webElementGuid>36309a17-4a0e-431f-889d-5299aa1ca336</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course berhasil ditambahkan ke pembelian!'])[1]/following::h2[1]</value>
      <webElementGuid>5218e6e2-ec05-4ada-bb89-6cdecc20368b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[2]/following::h2[1]</value>
      <webElementGuid>342ac6d6-952f-48bc-b515-8a4ef0c825fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Course Lainnya'])[1]/preceding::h2[1]</value>
      <webElementGuid>488e53b7-24e1-4488-8c29-29c444a8edf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/h2</value>
      <webElementGuid>d8418c2d-20ff-46f5-a02d-3b1728b37314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'modalEventTitle' and (text() = '
                    Mobile Engineer with React Native' or . = '
                    Mobile Engineer with React Native')]</value>
      <webElementGuid>61f75219-29db-4740-a51a-b49a7e33701d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
