<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_E-Mail_email</name>
   <tag></tag>
   <elementGuidId>4028683e-4274-4dc8-8149-d7e7400a3a8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>89df4fc7-b238-462f-a78e-8e583bdafb75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>b156c554-e898-4f36-bc18-5d8922571d92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>contoh@mail.com</value>
      <webElementGuid>a42d71af-013d-4bf2-bbde-be81d2060cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>7c486d38-d0f1-4741-99f0-7f1578cdc3b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>9d9071e6-9d94-4c94-8b95-6be6e5537693</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>699b5eee-6df0-464a-b680-a7ebffb333c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>fdef6a2e-4d83-42cc-b0d2-68fe60cb9e6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;email&quot;)</value>
      <webElementGuid>685aef99-4a1c-4967-a5b3-c3ceb8605bb9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='email']</value>
      <webElementGuid>b8192b87-c679-4303-828d-010851bc397c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/input</value>
      <webElementGuid>e5a34447-6260-406b-b2c0-28fecc5dd6c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'email' and @placeholder = 'contoh@mail.com' and @type = 'email' and @name = 'email']</value>
      <webElementGuid>3fa31fea-0364-456c-a2e0-f433a09ed16c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
